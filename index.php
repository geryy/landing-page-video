<!doctype html>
<html itemscope itemtype="http://schema.org/WebApp"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1">
	<title> #Resistance.Berlin</title>
	<link rel="canonical" href="http://recursion2.de" />
	<link rel="stylesheet" href="main.css?V2">
	<link href="https://plus.google.com/+GeraldScholz" rel="publisher" />
	<meta itemprop="name" content="4.Nov 2017 Ingress Anomaly Berlin">
	<meta itemprop="description" content="3 years after #Recursion a new #Ingress Anomaly will take place in Berlin">
	<meta itemprop="image" content="promo.png">
</head>
<body>
	
<a href="http://besmurf.de" target="_blank">
		<div class="logo">
		<h4 class="trs1"><span class="trs1 del4">be</span>smurf.de <br><span class="trs1 del6"> Resistance</span> Berlin</h4>
		<svg version="1.1" id="besmurf-logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			width="43.023px" height="60px" viewBox="0 0 43.023 60" style="enable-background:new 0 0 43.023 60;" xml:space="preserve">
			<path style="fill:#FFFFFF;" d="M43.017,22.839l-0.037-0.036L24.646,12.217v3.174l15.478,8.933l0.157,0.266v21.387l-0.104,0.17
				l-7.325,4.227l-6.37,3.676l-1.615,0.933l-1.091,0.629c-0.066-1.85-0.135-3.696-0.206-5.602c-0.293-7.845-0.594-15.951-0.696-23.861
				h0.323v-1.926c1.305-0.638,2.169-1.943,2.169-3.373c0-1.33-0.693-2.556-1.822-3.249l-0.099-0.465h-0.082v-3.251h0.167V12.66h-1.075
				l-0.083-0.414C22.367,11.894,22.09,5.301,21.883,0h-0.544c-0.242,5.4-0.531,12.249-0.531,12.249l-0.08,0.411h-1.072v1.224h0.167
				v3.251h-0.074l-0.109,0.462c-1.132,0.69-1.827,1.918-1.827,3.252c0,1.43,0.867,2.735,2.175,3.374v1.926h0.324
				c-0.101,7.796-0.4,15.786-0.692,23.518c-0.075,2.017-0.149,4.026-0.221,6.033l-1.092-0.631l-15.52-8.963l-0.042-0.071L2.743,24.733
				l0.27-0.477l15.53-8.949v-3.183L0.009,22.841L0,47.451l0.187,0.331l4.488,2.591l13.685,7.901L21.349,60l0.326-0.002l3.302-1.906
				l0,0l17.987-10.389l0.059-0.105L43.017,22.839z"/>
				<path class="trs1 logo-inner" style="fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;" d="M18.768,42.576l-6.662-11.584l6.906,3.963
					c0.029-1.049,0.057-2.097,0.081-3.144l-5.572-3.223l-0.005-0.042l0.019-0.055h5.627c0.007-0.41,0.016-0.821,0.022-1.231h-0.308
					v-1.113V25.78L5.965,25.783l-0.033,0.066l7.047,12.235l-0.016,0.076l-4.851,2.807l-0.021,0.075l1.217,2.116l0.332,0.083
					l4.693-2.697l0.077,0.02l4.168,7.206C18.642,46.048,18.707,44.315,18.768,42.576"/>
					<path class="trs1 logo-inner" style="fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;" d="M30.112,38.178l-0.043-0.154l7.003-12.138l-0.055-0.11
						L24.31,25.778v0.37v1.112h-0.309c0.008,0.41,0.016,0.821,0.022,1.231h5.453l0.02,0.057l-0.006,0.031l-0.029,0.035l-5.371,3.099
						c0.024,1.045,0.05,2.092,0.079,3.14l6.691-3.856l0.04,0.001l0.011,0.056l-6.508,11.249c0.061,1.742,0.123,3.479,0.187,5.204
						l3.989-6.902l0.141-0.042l4.766,2.742l0.17-0.052l1.271-2.207l-0.019-0.093L30.112,38.178z"/>
					</svg>
				</div>
</a>
				<section class="wrap">
						<div class="flex-center">
							<div class="event fadeIn" itemscope itemtype="http://schema.org/Event">
				<svg version="1.1" id="besmurf-logo2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			width="43.023px" height="60px" viewBox="0 0 43.023 60" style="enable-background:new 0 0 43.023 60;" xml:space="preserve">
			<path style="fill:#FFFFFF;" d="M43.017,22.839l-0.037-0.036L24.646,12.217v3.174l15.478,8.933l0.157,0.266v21.387l-0.104,0.17
				l-7.325,4.227l-6.37,3.676l-1.615,0.933l-1.091,0.629c-0.066-1.85-0.135-3.696-0.206-5.602c-0.293-7.845-0.594-15.951-0.696-23.861
				h0.323v-1.926c1.305-0.638,2.169-1.943,2.169-3.373c0-1.33-0.693-2.556-1.822-3.249l-0.099-0.465h-0.082v-3.251h0.167V12.66h-1.075
				l-0.083-0.414C22.367,11.894,22.09,5.301,21.883,0h-0.544c-0.242,5.4-0.531,12.249-0.531,12.249l-0.08,0.411h-1.072v1.224h0.167
				v3.251h-0.074l-0.109,0.462c-1.132,0.69-1.827,1.918-1.827,3.252c0,1.43,0.867,2.735,2.175,3.374v1.926h0.324
				c-0.101,7.796-0.4,15.786-0.692,23.518c-0.075,2.017-0.149,4.026-0.221,6.033l-1.092-0.631l-15.52-8.963l-0.042-0.071L2.743,24.733
				l0.27-0.477l15.53-8.949v-3.183L0.009,22.841L0,47.451l0.187,0.331l4.488,2.591l13.685,7.901L21.349,60l0.326-0.002l3.302-1.906
				l0,0l17.987-10.389l0.059-0.105L43.017,22.839z"/>
				<path class="trs1 logo-inner" style="fill-rule:evenodd;clip-rule:evenodd;" d="M18.768,42.576l-6.662-11.584l6.906,3.963
					c0.029-1.049,0.057-2.097,0.081-3.144l-5.572-3.223l-0.005-0.042l0.019-0.055h5.627c0.007-0.41,0.016-0.821,0.022-1.231h-0.308
					v-1.113V25.78L5.965,25.783l-0.033,0.066l7.047,12.235l-0.016,0.076l-4.851,2.807l-0.021,0.075l1.217,2.116l0.332,0.083
					l4.693-2.697l0.077,0.02l4.168,7.206C18.642,46.048,18.707,44.315,18.768,42.576"/>
					<path class="trs1 logo-inner" style="fill-rule:evenodd;clip-rule:evenodd;" d="M30.112,38.178l-0.043-0.154l7.003-12.138l-0.055-0.11
						L24.31,25.778v0.37v1.112h-0.309c0.008,0.41,0.016,0.821,0.022,1.231h5.453l0.02,0.057l-0.006,0.031l-0.029,0.035l-5.371,3.099
						c0.024,1.045,0.05,2.092,0.079,3.14l6.691-3.856l0.04,0.001l0.011,0.056l-6.508,11.249c0.061,1.742,0.123,3.479,0.187,5.204
						l3.989-6.902l0.141-0.042l4.766,2.742l0.17-0.052l1.271-2.207l-0.019-0.093L30.112,38.178z"/>
					</svg>
								<h2  itemprop="name">Berlin Anomaly</h2>
								<h3 itemprop="startDate" content="2017-11-04T12:00">4. November 2017</h3>
								<h5 class="fadeIn delay2">#<span>Recursion</span>2</h5>
								<ul class="countdown">
									<li>
										<p><span class="days">00</span></p>
										<p><em class="textDays">days</em></p>
									</li>
									<li>
										<p><span class="hours">00</span></p>
										<p><em class="textHours">hours</em></p>
									</li>
									<li>
										<p><span class="minutes">00</span></p>
										<p><em class="textMinutes">minutes</em></p>
									</li>
									<li>
										<p><span class="seconds">00</span></p>
										<p><em class="textSeconds">seconds</em></p>
									</li>
								</ul>
							<p>Watch the <a href="https://plus.google.com/u/0/s/%23recursion/top">#Recursion</a> 2014 recap <br>
							<a href="https://www.youtube.com/watch?v=ZAupAU5XO2o" target="_blank" class="btn">Berlin 1st Anomaly</a>
							</p>
						</div>
					</div>
					
				</section>
				<footer>
					<div class="left"> <div class="g-plusone" data-size="medium" expandTo="right" data-annotation="bubble" data-width="300"></div> </div>
					<!-- <div class="right"><a href="https://plus.google.com/+GeraldScholz/posts/EuYe2VXb1Qv">Google+</a></div> -->
				</footer>
				<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
				<script src="jquery.tubular.1.0.js"></script>
				<script src="counter.js"></script>
				<script src="main.js"> </script>
				<!-- big brother -->
				<script>
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

				  ga('create', 'UA-44659136-4', 'auto');
				  ga('send', 'pageview');
				</script>
			</body> 
		</html>